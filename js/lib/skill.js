// Define the skill module
define(['jquery','resume-component'],function($,ResumeComponent){
    // Skill is subclass of ResumeComponent
    var Skill = function (skillData) {
      // Call super constructor
      ResumeComponent.call(this);
      // Assign a nice helper variable
      var self = this;
      if (!skillData) {
        throw("You must supply this with a skill");
      }
      self.skill = skillData;
    };

    // Inherit via Object.create
    Skill.prototype = Object.create(ResumeComponent.prototype);
    Skill.prototype.constructor = Skill;

    Skill.prototype.toMarkdown = function(onComplete){
      var self = this;
      // Call the superclass version of this function
      ResumeComponent.call(this,arguments);

      self.renderTemplate('skill',self.skill,function(markdown){
        console.log("Rendered Skill to markdown",markdown);
        onComplete(markdown);
      });

    };

    return Skill;
  }
);
