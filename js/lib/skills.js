// Define the skills module
define(['jquery','resume-component'],function($,ResumeComponent){
    // Skill is subclass of ResumeComponent
    var Skills = function (skills) {
      // Call super constructor
      ResumeComponent.call(this);
      // Assign a nice helper variable
      var self = this;
      if (!skills) {
        throw("You must supply this with a skill");
      }
      self.skills = skills;
    };

    // Inherit via Object.create
    Skills.prototype = Object.create(ResumeComponent.prototype);
    Skills.prototype.constructor = Skills;

    Skills.prototype.toMarkdown = function(){
      var self = this;
      var promise = $.Deferred();
      // Call the superclass version of this function
      ResumeComponent.call(this,arguments);

      self.renderTemplate('skills',{skills:self.skills},function(markdown){
        promise.resolve(markdown);
      });

      return promise;
    };

    return Skills;
  }
);
