// Define the experience module
define(['jquery','resume-component'],function($,ResumeComponent){
    // experience is subclass of ResumeComponent
    var Experience = function (experienceData) {
      console.log("Inside Experience Constructor");
      // Call super constructor
      ResumeComponent.call(this);
      // Assign a nice helper variable
      var self = this;
      if (!experienceData) {
        throw("You must supply this with a experience");
      }
      self.experience = experienceData;
    };

    // Inherit via Object.create
    Experience.prototype = Object.create(ResumeComponent.prototype);
    Experience.prototype.constructor = Experience;

    Experience.prototype.toMarkdown = function(){
      var self = this;

      var promise = $.Deferred();
      // Call the superclass version of this function
      ResumeComponent.call(this,arguments);
      // Build out the markdown with mustache.js
      self.renderTemplate('experience',{experience:self.experience},function(markdown){
        console.log(markdown);
        promise.resolve(markdown);
      });
      return promise;
    };

    return Experience;
  }
);
