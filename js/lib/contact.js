// Define the contact module
define(['jquery','resume-component'],function($,ResumeComponent){
    // Contact is subclass of ResumeComponent
    var Contact = function (contactData) {
      // Call super constructor
      ResumeComponent.call(this);
      // Assign a nice helper variable
      var self = this;
      if (!contactData) {
        throw("You must supply this with a contact");
      }
      self.contact = contactData;
    };

    // Inherit via Object.create
    Contact.prototype = Object.create(ResumeComponent.prototype);
    Contact.prototype.constructor = Contact;

    Contact.prototype.toMarkdown = function(onComplete){
      var self = this;

      var promise = $.Deferred();
      // Call the superclass version of this function
      ResumeComponent.call(this,arguments);
      // Build out the markdown with mustache.js
      self.renderTemplate('contact',self.contact,function(markdown){
        promise.resolve(markdown);
      });
      return promise;
    };

    return Contact;
  }
);
