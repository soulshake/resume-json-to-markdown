// Define the resume module
define(['jquery', 'contact', 'skills','experience'], function($, Contact, Skills, Experience) {
  var Resume = function(resumeData) {
    console.log("Building Resume with ", resumeData);
    var self = this;
    if (!resumeData) {
      throw ("You must supply this with a resume in JSON format");
    }

    self.output = {
      Contact: "",
      Skills: []
    };

    self.resume = resumeData;
    self.skills = [];
    self.init();
  };

  Resume.prototype.init = function() {
    var self = this;
    if (self.resume.contact) {
      self.contact = new Contact(self.resume.contact);
    }
    if (self.resume.skills) {
      self.skills = new Skills(self.resume.skills);
    }
    if (self.resume.experience) {
      self.experience = new Experience(self.resume.experience);
    }
  };

  Resume.prototype.toMarkdown = function() {
    var self = this;
    var promise = $.Deferred();
    // Go fetch all the pieces
    var contactPromise = self.contact.toMarkdown();
    var skillsPromise = self.skills.toMarkdown();
    var experiencePromise = self.experience.toMarkdown();

    // When all of them are done
    $.when(contactPromise, skillsPromise,experiencePromise).done(function(contactMarkdown, skillsMarkdown,experienceMarkdown) {
      // Append all the markdown together
      var markdown = contactMarkdown;
      markdown += experienceMarkdown;
      markdown += skillsMarkdown;
      // Resolve the promise now that we have all the markdown
      promise.resolve(markdown);
    });
    // Return the promise
    return promise;
  };

  return Resume;
});