require.config({
  "baseUrl": "js/lib",
  "paths": {
    "app": "../app",
    "mustache": "mustache/mustache",
    "handlebars": "handlebars/handlebars-v1.3.0",
    "marked": "marked/marked",
    "promise": "promise/promise-3.2.0",
    "highlight": "highlight.pack"
  },
  shim: {
    'handlebars': {
      exports: 'Handlebars'
    }
  }
});

// Load the main app module to start the app
require(["app/resume", "jquery", "marked", "highlight"], function(Resume, $, marked, hljs) {
  var resumeRequest = $.get('resume.json');

  var tabs = $('.et_pb_tab');

  marked.setOptions({
    gfm:true,
    breaks:true,
    sanitize:false,
    tables:true,
    smartLists:true,
    smartypants:true,
    pedantic:true
  });

  var ResumeTab = tabs[0];
  var JSONTab = tabs[1];
  var MarkdownTab = tabs[2];
  var HTMLTab = tabs[3];

  var HTMLContainer = $('<code class="hljs">');
  var JSONContainer = $('<code class="hljs">');
  var MarkdownContainer = $('<code class="hljs">');

  $(JSONTab).append($('<pre>').append(JSONContainer));
  $(HTMLTab).append($('<pre>').append(HTMLContainer));
  $(MarkdownTab).append($('<pre>').append(MarkdownContainer));

  resumeRequest.done(function(data) {
    resume = new Resume(data);
    // Re-JSON the JSON to put it in the page
    var jsonPretty = JSON.stringify(data, null, "  ");
    var highlightedJSON = hljs.highlight('json', jsonPretty).value;
    JSONContainer.html(highlightedJSON);
    var promise = resume.toMarkdown();
    promise.done(function(markdown) {
      var highlightedMarkdown = hljs.highlight('markdown', markdown).value;
      MarkdownContainer.html(highlightedMarkdown);
      var html = marked(markdown);
      var highlightedHTML = hljs.highlight('html', html).value;
      HTMLContainer.html(highlightedHTML);
      $(ResumeTab).html(html);
    });
  });
});
